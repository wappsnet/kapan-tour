/**
 *
 * @param selector
 * @param direction
 * @returns {
     * element: stepDomNode
     * elementPosition: stepDomNode position
     * stepPosition: step modal place position
     * }
 */
function stepNode(selector, direction = 'top') {
    let stepDomObject = {};

    stepDomObject.direction = direction;
    stepDomObject.element = document.querySelector(selector);

    if(stepDomObject.element === null) {
        return null;
    }

    stepDomObject.elementRect = stepDomObject.element.getBoundingClientRect();

    //get the tour step position in document according to passed direction (left, top ...)
    switch (direction) {
        case 'top': {
            stepDomObject.position = {
                left: stepDomObject.elementRect.left + stepDomObject.elementRect.width/2,
                top: stepDomObject.elementRect.top
            };
            break;
        }

        case 'bottom': {
            stepDomObject.position = {
                left: stepDomObject.elementRect.left + stepDomObject.elementRect.width/2,
                top: stepDomObject.elementRect.top + stepDomObject.elementRect.height
            };
            break;
        }

        case 'left': {
            stepDomObject.position = {
                left: stepDomObject.elementRect.left,
                top: stepDomObject.elementRect.top + stepDomObject.elementRect.height/2
            };
            break;
        }

        case 'right': {
            stepDomObject.position = {
                left: stepDomObject.elementRect.left + stepDomObject.elementRect.width,
                top: stepDomObject.elementRect.top + stepDomObject.elementRect.height/2
            };
            break;
        }
    }

    return stepDomObject;
}

class stepModal {
    /**
     *
     * @param options {
     * appendTo: the dom node where the tour will be placed
     * containerId: tour container dom id
     * containerClasses: tour overlay classNames
     * }
     * @returns {ModalDom}
     */
    constructor(options) {
        this.options = {
            appendTo: options.appendTo || document.body,
            containerId: options.containerId || 'uc-tour',
            containerClasses: options.containerClasses || '',
        };

        return this;
    }

    /**
     * function createModalContainer
     */
    createModalContainer() {
        let modalContainer = document.getElementById(this.options.containerId);

        if(modalContainer === null) {
            modalContainer = document.createElement('div');
            modalContainer.id = this.options.containerId;
            modalContainer.classNames = this.options.containerClasses;

            this.options.appendTo.appendChild(modalContainer);
        }

        return modalContainer;
    }

    /**
     * function clearModalContainer
     */
    clearModalContainer() {
        let modalContainer = document.getElementById(this.options.containerId);

        if(modalContainer) {
            modalContainer.parentNode.removeChild(modalContainer);
        }
    }
}

export {
    stepNode,
    stepModal
}