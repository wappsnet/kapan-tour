import Component from '@ember/component';
import layout from '../templates/components/uc-tour-pointer';
import { htmlSafe } from '@ember/string';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'uc-tour-pointer'
    ],

    attributeBindings: [
        'style'
    ],

    style: computed('step.elementRect.{left,top,width,height}', function () {
        if(this.step.elementRect) {
            let styles = `left: ${this.step.elementRect.left + this.step.elementRect.width/2}px;`;
            styles += `top: ${this.step.elementRect.top + this.step.elementRect.height/2}px;`;
            styles += `width: ${this.step.elementRect.width}px;`;
            styles += `height: ${this.step.elementRect.width}px;`;

            return htmlSafe(styles);
        }
    }),
});
