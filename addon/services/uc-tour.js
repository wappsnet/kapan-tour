import Service from '@ember/service';
import { computed, set } from '@ember/object';
import { later, cancel } from '@ember/runloop';
import { stepModal, stepNode } from '../utils/modal-dom';

export default Service.extend({
    tourObject: null,
    tourSteps: [],

    activeStepId: null,

    activeStepIndex: computed('activeStepId', 'tourSteps.[]', function() {
        let activeStepObject = this.getStepObject(this.activeStepId);

        return this.tourSteps.indexOf(activeStepObject);
    }),

    /**
     * tour current step data
     */
    activeStepObject: computed('activeStepId', function() {
        return this.getStepObject(this.activeStepId);
    }),

    /**
     * initialize tour
     * @param options = {
     * tourClass: tour dom object
     * stepFireEvent: default event name at which the tour step fired (click,mouseup)
     * }
     */
    initTour(options = {}) {
        if(!this.tourObject) {
            this.set('tourObject', {
                tourClass: new stepModal({
                    appendTo: options.appendTo,
                    containerId: options.containerId,
                    containerClasses: options.containerClasses,
                }),

                stepFireEvent: options.stepFireEvent || 'click',
                stepChangeDelay: options.stepChangeDelay || 100,
                modalClasses: options.modalClasses,
                tourIsActive: false
            });

            this.addSteps(options.steps);
        }
    },

    /**
     * create tour step object with predefined model
     * @param options
     * @returns {
     * id: the uniq identificator of tour step
     * stepFireEvent: event name at which the tour step will fire (null || click, mouseup)
     * classes: tour step modal classNames
     * buttons: tour step buttons (next, prev or cancel)
     * }
     */
    createStepObject(options = {}) {
        if(!options.id) {
            return null;
        }

        if(!options.stepFireEvent) {
            if (options.stepFireEvent !== null) {
                options.stepFireEvent = this.tourObject.stepFireEvent;
            }
        }

        if(!options.classes) {
            options.classes = this.tourObject.modalClasses;
        }

        if(!options.buttons || !options.buttons.length) {
            options.buttons = [
                {
                    name: 'back',
                    label: 'back',
                    action: this.back.bind(this),
                    onBack: null
                },
                {
                    name: 'next',
                    label: 'next',
                    action: this.next.bind(this),
                    onBack: null
                }
            ]
        }

        options.buttons.map((steButton) => {
            steButton.active = true;
        });

        if(!options.activate) {
            options.activate = (stepId) => {
                let stepObject = this.getStepObject(stepId);
                let stepDomData = stepNode(stepObject.selector, stepObject.direction);

                this.updateStep(stepId, stepDomData);
            }
        }

        options.next = this.next.bind(this);
        options.back = this.back.bind(this);

        return options;
    },

    /**
     * add steps to current tour
     * @param steps -> array of steps
     */
    addSteps(steps = []) {
        steps.map((step) => {
            let stepItem = this.createStepObject(step);

            if(stepItem) {
                this.tourSteps.pushObject(stepItem);
            }
        });
    },

    /**
     * update step data
     * @param stepId
     * @param updateData
     */
    updateStep(stepId, updateData = {}) {
        let stepObject = this.getStepObject(stepId);

        Object.keys(updateData).map((stepOptionName) => {
            set(stepObject, stepOptionName, updateData[stepOptionName]);
        });
    },

    /**
     * change step object by id
     * @param stepId
     */
    changeStepById(stepId) {
        cancel(this.timoutId);

        this.set('tourObject.tourIsActive', false);
        this.set('activeStepId', stepId);

        this.timoutId = later(() => {
            this.set('tourObject.tourIsActive', true);
        }, this.tourObject.stepChangeDelay);
    },

    /**
     * get step object by id
     * @param stepId
     */
    getStepObject(stepId) {
        return this.tourSteps.findBy('id', stepId);
    },

    /**
     * drive tour to next step
     */
    next() {
        let lastStepIndex = this.tourSteps.length - 1;
        let nextStepIndex = this.activeStepIndex + 1;
        let nextStepObject = this.tourSteps[nextStepIndex];

        if(nextStepObject) {
            let nextStepId = this.tourSteps[nextStepIndex].id;
            this.changeStepById(nextStepId);
        }

        if(nextStepIndex > lastStepIndex) {
            this.finish();
        }
    },

    /**
     * drive tour to prev step
     */
    back() {
        let firstStepIndex = 0;
        let prevStepIndex = Math.max(this.activeStepIndex - 1, firstStepIndex);
        let prevStepObject = this.tourSteps[prevStepIndex];

        this.changeStepById(prevStepObject.id);

        if(prevStepIndex <= 0) {
            let backButton = prevStepObject.buttons.findBy('name', 'back');

            if(backButton) {
                set(backButton, 'active', false);
            }
        }
    },

    /**
     * cancel tour
     */
    cancel() {
        this.set('tourObject.tourIsActive', false);
    },

    /**
     * start tour by default at first step
     */
    start() {
        this.set('tourObject.tourIsActive', true);
        this.changeStepById(this.tourSteps[0].id);
    },

    /**
     * cancel tour and remove tour object from dom
     */
    finish() {
        this.set('tourObject.tourIsActive', false);
        this.changeStepById(null);
    }
});
