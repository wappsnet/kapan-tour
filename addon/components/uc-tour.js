import Component from '@ember/component';
import layout from '../templates/components/uc-tour';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    ucTour: service(),

    tagName: '',

    /**
     * tour step data
     */
    step: computed.alias('ucTour.activeStepObject'),

    /**
     * show if tour is in active state (have active step)
     */
    tourIsActive: computed.alias('ucTour.tourObject.tourIsActive'),
});
