import Component from '@ember/component';
import layout from '../templates/components/uc-tour-step';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/string';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'uc-tour-modal'
    ],

    attributeBindings: [
        'style'
    ],

    style: computed('step.position.{left,top}', function () {
        if(this.step.position) {
            let styles = `left: ${this.step.position.left}px;`;
            styles += `top: ${this.step.position.top}px;`;

            return htmlSafe(styles);
        }
    }),

    didInsertElement() {
        this._super(...arguments);

        if(!this.step.position) {
            this.step.activate(this.step.id);
        }

        if(this.step.stepFireEvent) {
            this.step.element.addEventListener(this.step.stepFireEvent, this._fireStep =() => {
                this.step.next();
            });
        }
    },

    willDestroyElement() {
        this._super(...arguments);
        
        this.step.element.removeEventListener(this.step.stepFireEvent, this._fireStep);
    },

    actions: {
        clickTourButton(buttonData) {
            if(typeof buttonData.action === 'function') {
                buttonData.action();
            }
        }
    }
});
